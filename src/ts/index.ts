//import {ResponseFormat} from "tst_response";
import express from "express";
import * as response from "response";
import * as bodyParser from "body-parser";
import * as path from "path";
import { RequestHandler } from "express-serve-static-core";
import { NextFunction } from "connect";
export interface Middleware {


}

export class EasyHttp {
    res: Response;
    protected http = express();
    constructor()
    {
        this.res = new Response(path.resolve(__dirname + "../errorcode/errorcode.json"));
        //this.use_ejs();
    }
    //postを定義する前に呼ぶ
    use_post() 
    {
        this.http.use(bodyParser.urlencoded({ extended: true }));
        this.http.use(bodyParser.json());
    }
    //
    use_log()
    {
        this.http.use((req, res, next)=> 
        {
            console.log("[ HTTP ] " + req.body);
            next();
        });
    }
    use_ejs()
    {
        // テンプレートエンジンを EJS に設定
        this.http.set("view engine", "ejs");
        this.http.set('views', './views');
    }
    use_errorhandler()
    {

    }
    use(then:NextFunction)
    {
        this.http.use((req, res, next) => {
            then();
            next();
        });
    }
    get_api<T>(addr:string, params:T)
    {
        this.http.get(addr, (req, res)=>{
            res.json(params);
        });
    }
    get_render_params<T extends object>(addr: string, page:string, params:T)
    {
        this.http.get(addr, (req, res) => {
            res.render(page,params as object);
        });
    }
    get_render(addr: string, page: string) {
        this.http.get(addr, (req, res) => {
            res.render(page);
        });
    }
    post_api<Recv,Send>(addr: string,then:(recv:Recv)=>Send )
    {
        this.http.post(addr, function (req, res) {

            let rjson:Recv = req.body as Recv;
            let sjson:Send = then(rjson);
            res.json(sjson);
        });
    }
    listen(port:number,then:(port:number)=>void) {
        this.http.listen(port,()=>{
            then(port);
        });
    }
}
