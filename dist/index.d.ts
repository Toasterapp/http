import { NextFunction } from "connect";
export interface Middleware {
}
export declare class EasyHttp {
    res: Response;
    protected http: import("express-serve-static-core").Express;
    constructor();
    use_post(): void;
    use_log(): void;
    use_ejs(): void;
    use_errorhandler(): void;
    use(then: NextFunction): void;
    get_api<T>(addr: string, params: T): void;
    get_render_params<T extends object>(addr: string, page: string, params: T): void;
    get_render(addr: string, page: string): void;
    post_api<Recv, Send>(addr: string, then: (recv: Recv) => Send): void;
    listen(port: number, then: (port: number) => void): void;
}
