"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
//import {ResponseFormat} from "tst_response";
var express_1 = __importDefault(require("express"));
var bodyParser = __importStar(require("body-parser"));
var path = __importStar(require("path"));
var EasyHttp = /** @class */ (function () {
    function EasyHttp() {
        this.http = express_1.default();
        this.res = new Response(path.resolve(__dirname + "../errorcode/errorcode.json"));
        //this.use_ejs();
    }
    //postを定義する前に呼ぶ
    EasyHttp.prototype.use_post = function () {
        this.http.use(bodyParser.urlencoded({ extended: true }));
        this.http.use(bodyParser.json());
    };
    //
    EasyHttp.prototype.use_log = function () {
        this.http.use(function (req, res, next) {
            console.log("[ HTTP ] " + req.body);
            next();
        });
    };
    EasyHttp.prototype.use_ejs = function () {
        // テンプレートエンジンを EJS に設定
        this.http.set("view engine", "ejs");
        this.http.set('views', './views');
    };
    EasyHttp.prototype.use_errorhandler = function () {
    };
    EasyHttp.prototype.use = function (then) {
        this.http.use(function (req, res, next) {
            then();
            next();
        });
    };
    EasyHttp.prototype.get_api = function (addr, params) {
        this.http.get(addr, function (req, res) {
            res.json(params);
        });
    };
    EasyHttp.prototype.get_render_params = function (addr, page, params) {
        this.http.get(addr, function (req, res) {
            res.render(page, params);
        });
    };
    EasyHttp.prototype.get_render = function (addr, page) {
        this.http.get(addr, function (req, res) {
            res.render(page);
        });
    };
    EasyHttp.prototype.post_api = function (addr, then) {
        this.http.post(addr, function (req, res) {
            var rjson = req.body;
            var sjson = then(rjson);
            res.json(sjson);
        });
    };
    EasyHttp.prototype.listen = function (port, then) {
        this.http.listen(port, function () {
            then(port);
        });
    };
    return EasyHttp;
}());
exports.EasyHttp = EasyHttp;
